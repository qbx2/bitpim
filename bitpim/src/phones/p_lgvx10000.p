### BITPIM ( -*- Python -*- )
###
### Copyright (C) 2007-2008 Nathan Hjelm <hjelmn@users.sourceforge.net>
###
### This program is free software; you can redistribute it and/or modify
### it under the terms of the BitPim license as detailed in the LICENSE file.
###
### $Id: p_lgvx10000.p 4639 2008-07-22 19:50:21Z djpham $

%{

"""Various descriptions of data specific to LG VX10000"""

from p_lgvx8800 import *

# same as VX-8800

%}
