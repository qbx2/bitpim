#!/usr/bin/env python

### BITPIM
###
### Copyright (C) 2005 Joe Pham <djpham@bitpim.org>
### Copyright (C) 2005 Roger Binns <rogerb@bitpim.org>
###
### This program is free software; you can redistribute it and/or modify
### it under the terms of the BitPim license as detailed in the LICENSE file.
###
### $Id: announce.py 3832 2006-12-31 01:17:52Z rogerb $


from email.Utils import formatdate
import smtplib
import sys
import time
import re
import os
import quopri

release_notes_filename='site/help/versionhistory.htm'
from_addr='announcements@bitpim.org'
to_addr='bitpim-announce@lists.sourceforge.net'
body_html="""
<html>
<body>
<p>Grab it from:</p>

<p><blockquote>
  <a href='http://www.bitpim.org/#download-devel'>http://www.bitpim.org/#download-devel</a>
</blockquote></p>

<p>BitPim can also notify you of updates.  This functionality is off by default.
You can enable it in the Settings menu.</p>

"""


# parse release notes to find version number and notes
verline=re.compile("\\s*<h2><a name=['\"]([0-9]+\\.[0-9]+\\.[0-9]+)['\"]>Changes in .*</a></h2>.*", re.IGNORECASE|re.DOTALL)
version=None
notes=[]
for line in open(release_notes_filename, "rt"):
    mo=verline.match(line)
    if mo:
        if version:
            break
        version=mo.group(1)
    if version:
        notes.append(line)

body_html+=("".join(notes))
body_html+="</body></html>"

headers="From: BitPim Announcer <%s>\nTo: BitPim Announcements <%s>\nDate: %s\nSubject: BitPim %s available\n" % \
          (from_addr, to_addr, formatdate(time.time(), True), version)


if sys.platform=='win32':
    def dumphtml(source):
        tmpfile=open("c:/temp/bpannounce.html", "wt")
        tmpfile.write(source)
        tmpfile.close()
        os.system("c:/bin/links/links.exe -dump \\temp\\bpannounce.html > c:/temp/bpannounce.txt")
        os.remove("c:/temp/bpannounce.html")
        res=open("c:/temp/bpannounce.txt", "rt").read()
        os.remove("c:/temp/bpannounce.txt")
        return res
elif sys.platform=='linux2':
    def dumphtml(source):
        tmpfile=open("/tmp/bpannounce.html", "wt")
        tmpfile.write(source)
        tmpfile.close()
        os.system("links -dump /tmp/bpannounce.html > /tmp/bpannounce.txt")
        os.remove("/tmp/bpannounce.html")
        res=open("/tmp/bpannounce.txt", "rt").read()
        os.remove("/tmp/bpannounce.txt")
        return res
else:
    print "You need to write an html to text convertor function"
    sys.exit(1)

body_text=dumphtml(body_html)

print body_text

# The stupid python mime object library is far too obtuse and poorly documented to figure out how
# to make a multi part alternative, so I do it by hand.

boundary="----=_NextPart_"+`time.time()`  # never likely to occur for real

message="%sMIME-Version: 1.0\nContent-Type: multipart/alternative; boundary=\"%s\"\n\n%s\n"
message+="--%s\nContent-Type: text/plain; charset=\"iso-8859-1\"\nContent-Transfer-Encoding: quoted-printable\n\n%s\n--%s\n"
message+="Content-Type: text/html; charset=\"iso-8859-1\"\nContent-Transfer-Encoding: quoted-printable\n\n%s\n--%s--\n\n\n\n\n"

qp=quopri.encodestring

message=message % (
    headers, boundary, body_text, # we put in body_text instead of the customary line saying it is a mime-message.  that will help really old readers.
    boundary, qp(body_text), boundary,
    qp(body_html), boundary)

# send to the specified SMTP server
if len(sys.argv)==2:
    smtp=smtplib.SMTP(sys.argv[1])
    print "Send Message [Ny]",
    x=raw_input()
    if x.lower().startswith("y"):
        smtp.sendmail(from_addr, to_addr, message)
        smtp.quit()

