#!/bin/sh

# This also runs on Windows.  Unfortunately rsync hasn't been ported to
# mingw (it only runs under cygwin), and so we can't use plink and
# the putty agent
# 

# mingw sh mangles the last parameter believing it to be a windows
# pathname (: converted to ; ,  / path converted to start with c:\)
# so we have to double up the //


# we have to delete any local emacs backup files.  this is because if
# we use an exclude pattern of "*~" it gets all sorts of quoting and
# requoting due to this being a mingw shell running rsync which is a
# cywgin program, and all that under windows!
export PATH=/usr/bin:$PATH
find . -name "*~" | xargs -rt rm

set -x

rsync --rsh=ssh --checksum --recursive --times --exclude CVS --exclude .cvsignore --exclude .svn --exclude Wiki --delete --compress --verbose site/ rogerb@www.bitpim.org://home/groups/b/bi/bitpim/htdocs/

ssh rogerb@www.bitpim.org "cd /home/groups/b/bi/bitpim/htdocs/ ; chmod -R g+w . ; chmod -R a+r . ; find . -type d -print0 | xargs -0 chmod a+rx ; ln -s cvschangelogbuilder_bitpim.html cvslog/index.html"
