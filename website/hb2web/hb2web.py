#!/usr/bin/env python

### HB2WEB.PY
license="""
### Copyright (C) 2006 Roger Binns <rogerb@rogerbinns.com>
###
###  This software is provided 'as-is', without any express or implied
###  warranty.  In no event will the authors be held liable for any
###  damages arising from the use of this software.
### 
###  Permission is granted to anyone to use this software for any
###  purpose, including commercial applications, and to alter it and
###  redistribute it freely, subject to the following restrictions:
### 
###  1. The origin of this software must not be misrepresented; you must
###     not claim that you wrote the original software. If you use this
###     software in a product, an acknowledgment in the product
###     documentation would be appreciated but is not required.
###
###  2. Altered source versions must be plainly marked as such, and must
###     not be misrepresented as being the original software.
###
###  3. This notice may not be removed or altered from any source
###     distribution.
###
### Note that some of the assets that come with this program are under
### their own license.  This includes the tree and the images it uses.
###
###
### $Id: hb2web.py 4548 2008-01-05 07:01:24Z djpham $
"""

# Variables in the templates
#
# @@helpnavtreeframename@@  -- name of the frame containing the nav tree 
# @@cookiename@@            -- name of the cookie used to talk between frames
# @@treeitems@@             -- JS code populating the tree
# @@title@@                 -- Title of the help
# @@mainpage@@              -- The initial page to display

import os, sys, optparse, re, copy, zipfile, HTMLParser, fnmatch, shutil, ConfigParser, StringIO

j=os.path.join

# setup where our default resources are
if os.path.isfile(sys.path[0]):
    resourcedirectory=os.path.abspath(j(os.path.dirname(sys.path[0]), "resources"))
else:
    resourcedirectory=os.path.abspath(j(sys.path[0], "resources"))

imgdirectory=j(resourcedirectory, "img")

###
### Parse command line options, open files etc
###
def main():
    # option type checker that something is an identifier

    def check_identifier(option, opt, value):
        if re.match("^[0-9A-Za-z_]+$", value):
            return value
        raise optparse.OptionValueError("option %s: the value %s should be a simple identifier (alphanumerics)" % (opt, value))

    def check_directory(option, opt, value):
        if os.path.isdir(value):
            return value
        raise optparse.OptionValueError("option %s: You need to supply a directory instead of %s" % (opt, value))

    class MyOption(optparse.Option):
        TYPES = optparse.Option.TYPES + ( "identifier", "directory" )
        TYPE_CHECKER = copy.copy(optparse.Option.TYPE_CHECKER)
        TYPE_CHECKER["identifier"]=check_identifier
        TYPE_CHECKER["directory"]=check_directory

    # actually get the options

    # %default didn't work for me, so help text specifies it manually
    parser=optparse.OptionParser("usage: %prog [options] inputfile.htb outputdirectory", option_class=MyOption)
    parser.add_option("-t", "--template-directory", metavar="DIR", type="directory",
                      help="Directory containing templates ["+resourcedirectory+"]",
                      default=resourcedirectory)
    parser.add_option("-i", "--image-directory", metavar="DIR", type="directory",
                      help="Directory containing images ["+imgdirectory+"]",
                      default=imgdirectory)
    parser.add_option("-n", "--frame-name", metavar="NAME",
                      help="Frame name [hb2webframe]", type="identifier",
                      default="hb2webframe")
    parser.add_option("-c", "--cookie-name", metavar="NAME",
                      help="cookie name [hb2webshowid]", type="identifier",
                      default="hb2webshowid")
    parser.add_option("--colour", "--color", help="Colour for top banner [#dcdcdc]", default="#DCDCDC")
    parser.add_option("--license", help="Show license", action="store_true", default=False)


    options, args = parser.parse_args()

    if options.license:
        print license
        sys.exit(0)

    # verify the passed in options

    if len(args)!=2:
        parser.error("You must specify the input file and output directory")

    try:
        htbfile=zipfile.ZipFile(args[0], "r")
    except:
        print "Failed to open the .htb file for reading"
        raise

    bad=htbfile.testzip()
    if bad:
        print "The htb file is bad.  The first bad file is",bad
        raise ValueError("Bad htb file")

    if not os.path.exists(args[1]):
        print "Creating output directory", args[1]
        try:
            os.makedirs(args[1])
        except:
            "Failed to create output directory", args[1]
            raise

    if not os.path.isdir(args[1]):
        print "Output location \""+args[1]+"\" is not a directory"
        raise ValueError("Output location is not a directory")

    process(htbfile, args[1], options)


class HHCParser(HTMLParser.HTMLParser):
    "A class that parses the hierarchy contents.  The .hhc files are nice and simple."
    def __init__(self):
        HTMLParser.HTMLParser.__init__(self)
        self.seenul=False

    def handle_starttag(self, tag, attrs):
        getattr(self, "handle_start_"+tag)(attrs)

    def handle_endtag(self, tag):
        getattr(self, "handle_end_"+tag)()

    def handle_end_body(self):
        pass

    def handle_end_html(self):
        pass

    def handle_start_html(self, attrs):
        pass

    def handle_start_head(self, attrs):
        pass
    
    def handle_end_head(self):
        pass

    def handle_start_meta(self, attrs):
        pass

    def handle_start_body(self, attrs):
        self.hier=[]
        self.parent=[]

    def handle_start_ul(self, attrs):
        self.seenul=True
        self.hier.append([])

    def handle_end_ul(self):
        if len(self.hier)>1:
            self.hier[-2].append(self.hier[-1])
            self.hier.pop()

    def handle_start_li(self, attrs):
        self.item={}

    def handle_start_object(self, attrs):
        pass

    def handle_start_param(self, attrs):
        if not self.seenul: return
        name=None
        value=None
        for a,v in attrs:
            if a.lower()=='name':
                name=v.lower()
            elif a.lower()=='value':
                value=v
        assert name is not None and value is not None
        self.item[name]=value

    def handle_end_object(self):
        if not self.seenul: return
        self.hier[-1].append(self.item)

def escape(name):
    "Ensures name is valid in Javascript strings"
    return name.replace("\\", "\\\\").replace("'", "\\'")

def genjstree(hier, parent=1, num=2):
    "Generates the JavaScript code to populate the tree"
    res=[]
    for i in hier:
        if isinstance(i, type([])):
            children=genjstree(i, num-1, num)
            res.extend(children)
            num+=len(children)
        else:
            res.append("helptoctree.add(%d,%d,'%s','%s','%s');" %
                       (num, parent, escape(i['name']), i['local'], escape(i['name'])))
            i['id']=num
            num+=1
    return res

def genidlist(hier):
    "Returns a dict mapping filenames to their ids.  You must have run genjstreee on the hier first."
    res={}

    for i in hier:
        if isinstance(i, type([])):
            res2=genidlist(i)
            res.update(res2)
        else:
            res[i['local']]=i['id']

    return res

def getfilenames(zipfile, pattern):
    "Returns a list of filenames in the zipfile matching the (case insensitive) glob style pattern"
    return [f for f in zipfile.namelist() if fnmatch.fnmatch(f, pattern)]

def substitute(data, patterns):
    for k,v in patterns.items():
        data=data.replace(k,v)
    return data
        

def process(htbfile, opdir, options):
    "Do the actual work for one htbfile and output directory"
    # Get the hierarchy from the hhc file
    parser=HHCParser()
    hhcs=getfilenames(htbfile, "*.hhc")
    if len(hhcs)==0:
        print "No .hhc file found in the htb.  This file is needed for the table"
        print "of contents"
        raise ValueError("No .hhc present")
    elif len(hhcs)>1:
        print "More than one .hhc file found in the .htb.  Only the first one is used."
    print "Using HHC",hhcs[0]
    parser.feed(htbfile.read(hhcs[0]))
    parser.close()
    parser.hier=parser.hier[0]

    # Get settings from hhp file
    hhps=getfilenames(htbfile, "*.hhp")
    if len(hhps)==0:
        print "No .hhp file found in the htb.  This file is needed for settings"
        raise ValueError("No .hhp present")
    elif len(hhps)>1:
        print "More than one .hhp file found in the .htb.  Only the first one is used."
    print "Using HHP",hhps[0]
    cfg=ConfigParser.ConfigParser()
    res=[]
    cfg.readfp(StringIO.StringIO(hhpfixup(htbfile.read(hhps[0]))), hhps[0])
    # some idiot who implemented ConfigParser decided the section names are case sensitive
    # and options are case insensitive.  This works around that to work out how the
    # options section is spelt
    optionssection="OPTIONS" # hb default
    for n in cfg.sections():
        if n.lower()==optionssection.lower():
            optionssection=n
            break

    # setup substitutions
    subst={}
    subst["@@treeitems@@"]="\n".join(genjstree(parser.hier))
    subst["@@helpnavtreeframename@@"]=options.frame_name
    subst["@@cookiename@@"]=options.cookie_name
    subst["@@title@@"]=cfg.get(optionssection, "title", True)
    subst["@@mainpage@@"]=cfg.get(optionssection, "Default topic", True)
    subst["@@colour@@"]=options.colour

    idlist=genidlist(parser.hier)
    
    # copy the contents of the htb to the output directory
    names=htbfile.namelist()
    names.sort()
    for n in names:
        data=htbfile.read(n)
        if n in idlist:
            data=substitute(data, subst)
            data=applyjs(data, idlist[n], n)
            print "A",n
        else:
            print " ",n
        open(j(opdir, n), "wb").write(data)

    # images
    imgpat=re.compile(r"^.*\.gif$", re.IGNORECASE)
    imgdir=j(opdir, "img")
    if not os.path.isdir(imgdir):
        os.mkdir(imgdir)
    for n in os.listdir(imgdirectory):
        if imgpat.match(n):
            if options.image_directory!=imgdirectory and os.path.isfile(j(options.image_directory, n)):
                src=j(options.image_directory)
                print "*",n
            else:
                src=j(imgdirectory, n)
                print "I",n
            shutil.copyfile(src, j(imgdir, n))

    # The remaining files
    filespat=re.compile(r"^.*\.(css|js|html)$", re.IGNORECASE)
    for n in os.listdir(resourcedirectory):
        if filespat.match(n):
            if options.template_directory!=resourcedirectory and os.path.isfile(j(options.template_directory, n)):
                src=j(options.template_directory)
                print "*",n
            else:
                src=j(resourcedirectory, n)
                print "T",n
            data=open(src, "rb").read()
            open(j(opdir, n), "wb").write(substitute(data, subst))

def hhpfixup(data):
    # ConfigParser requires every line to have an = or be a section, but hhp doesn't do that for the file
    # listing, so we have to fake it
    res=[]
    for line in StringIO.StringIO(data):
        line=line.strip()
        if len(line):
            if not (line.startswith("[") or line.find("=")>0):
                line='file='+line
            res.append(line)
    return "\n".join(res)

        
bodyclose=re.compile(r"(.*)<\s*/body\s*>(.*)", re.IGNORECASE|re.DOTALL)

def applyjs(data, id, fname):
    mo=bodyclose.match(data)
    if not mo:
        print "Couldn't find the close body </body> tag in",fname
        raise ValueError("No body close")
    nd=mo.group(1)+getjsloadfunc(id)+"</body>"+mo.group(2)
    assert(len(nd)>len(data))
    
    return nd

def getjsloadfunc(id):
    # see http://blog.firetree.net/2005/07/17/javascript-onload/
    return '\n<script language="JavaScript" src="hb2web.js"></script>\n'+ \
           '<script language="JavaScript">\nfunction hb2webonload() { var prev=window.onload; window.onload=function() { if(prev) prev(); if (frameme(%d)) {showinhelpcontents(%d);}}}\nhb2webonload();\n</script>\n' \
           % (id, id)

if __name__=='__main__':
    main()




