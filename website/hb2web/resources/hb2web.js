//
//  The initial section of code dealing with cookies comes from
//  http://www.webreference.com/js/column8/functions.html
//  You can find exactly the same code in thousands of other places on the net


// name - name of the cookie
// value - value of the cookie
// [expires] - expiration date of the cookie (defaults to end of current session)
// [path] - path for which the cookie is valid (defaults to path of calling document)
// [domain] - domain for which the cookie is valid (defaults to domain of calling document)
// [secure] - Boolean value indicating if the cookie transmission requires a secure transmission
// * an argument defaults when it is assigned null as a placeholder
// * a null placeholder is not required for trailing omitted arguments
function setCookie(name, value, expires, path, domain, secure) {
  var curCookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires.toGMTString() : "") +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
  document.cookie = curCookie;
}

// name - name of the desired cookie
// * return string containing value of specified cookie or null if cookie does not exist
function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return unescape(dc.substring(begin + prefix.length, end));
}

// name - name of the cookie
// [path] - path of the cookie (must be same as path used to create cookie)
// [domain] - domain of the cookie (must be same as domain used to create cookie)
// * path and domain default if assigned null or omitted if no explicit argument proceeds
function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    document.cookie = name + "=" + 
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}

// date - any instance of the Date object
// * hand all instances of the Date object to this function for "repairs"
function fixDate(date) {
  var base = new Date(0);
  var skew = base.getTime();
  if (skew > 0)
    date.setTime(date.getTime() - skew);
}

//
//  My own code from this point on
//

// Note that location.replace is used throughout.  This causes the
// right thing to happen when users press the back button.

// Finds the navtree and makes it mark this page as the currently
// selected one.
function showinhelpcontents(id) {
    for (var i=0;i<parent.frames.length;i++) {
      // look for the correct frame
      if (parent.frames[i].name == '@@helpnavtreeframename@@')
	{
	  var tree=parent.@@helpnavtreeframename@@.helptoctree;
	  // The tree may be null if it hasn't finished loading yet.  That can happen if
	  // the frameset has just opened and the various pages have only just been
	  // retrieved
	  if (tree)
	    tree.openTo(id, true);
	  else
	    // ok, try again in half a second
	    setTimeout("showinhelpcontents("+id+");", 500)
	}
    }
}

// Returns true if we are already part of frames
// otherwise causes the frameset to load, and returns false
function frameme(id) {
  if (parent.frames.length==0) {
    var currenturl=location.href;
    // location.hash is any part after # (including the hash sign)
    setCookie("@@cookiename@@", id+location.hash);
    // we assume the frameset can be found by stripping everything off after the last /
    currenturl=currenturl.substring(0, currenturl.lastIndexOf('/')+1);
    location.replace(currenturl);
    return false;
  }
  return true;
}

// See if we can find the cookie.
// If it is found then we display the corresponding page
function checkforhb2webcookie() {
  // find the cookie
  var id=getCookie("@@cookiename@@");
  if (id!=null) {
    // split the id into the actual id and the hash
    var hash="";
    var hashpos=id.lastIndexOf('#')
    if (hashpos>0)
      {
	hash=id.substring(hashpos, id.length);
	id=id.substring(0, hashpos);
      }
    // cause the tree to show the right bit
    self.helptoctree.openTo(id, true);
    // we don't need the cookie anymore
    deleteCookie("@@cookiename@@");
    // no idea why this fudge is needed
    id=id-1;
    // find the target and URL, and display them
    var target=self.helptoctree.aNodes[id].target;
    var url=self.helptoctree.aNodes[id].url+hash;
    for (var i=0;i<parent.frames.length;i++) {
      if (parent.frames[i].name == target)
	{
	  parent.frames[i].location.replace(url);
	  break;
	}
    }
  }
}
